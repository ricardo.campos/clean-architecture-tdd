const { expect } = require('chai')
const postUseCase = require('../../../src/domain/post/usecase.js')()

describe('Post domain', () => {
    
  it('Post have Body and Title', () => {
    return postUseCase.getPost()
    .then(post => expect(post).to.have.property('title') && expect(post).to.have.property('body'))  
  })

  it('Post body and title are strings', () => {
    return postUseCase.getPost()
    .then(post => expect(post.title).to.be.a('string') && expect(post.body).to.be.a('string'))  
  })
})
